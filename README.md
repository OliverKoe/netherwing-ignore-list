# Requirements
1. git
2. [IgnoreMore][]  Addon
3. on Linux : wine (duh)

# How To 
## Install on Linux
1. Clone this Repository to /PATH/TO/YOUR/WOW/INSTANCE/WTF/Account/$YOUR_ACCOUNT_NAME/SavedVariables
2. Open wow.sh in an editor of choice
3. Fill in the values for $WOWPATH and $ACCOUNTNAME
4. Use the script to launch your client

## Ignoring someone
1. Screencap the Offender
2. Go to the [BitBucket][] issue tracker of this repository
3. Upload the screenshot and maybe some description
[IgnoreMore]: https://www.wowace.com/projects/ignore-more/files/132847
[BitBucket]: https://bitbucket.org/OliverKoe/netherwing-ignore-list/issues/new
